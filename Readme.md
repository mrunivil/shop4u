# Shop4U - Our Solution Against Coronavirus
Visit us at https://shop4u.tk/ (on your mobile phone)
## Problem Statement
As a project team of the #WirVsVirus Hackathon we asked us:  
How can we realise a digitial shoppinglist to minimize the risk of infection with the coronavirus while shopping in a supermarket?  

The solution? -> Shop4U, a simple way to create and share your shoppinglist!

## How does it stop the virus spreading?
Shop4U minimizes the time you spent in the market. You don't have to touch any items in the supermarket and there is no contact with any other person except the cashier. So let's protect ourselves and the supermarket team with this concept.

## How does it work?
1.  Start with adding all the items to your list.
2.  Choose the supermarket you want your items from.
3.  Send your shoppinglist to the supermarket.
4.  The supermarket team will collect the items for you.
5.  You can go to the market pay and receive everything.

## What else?
If you are part of a vulnerable group of this virus, we will find someone to shop for you! Simply create your list and share it with someone you know on WhatsApp. Or find a nearby volunteer in the app who wants to help you out.

![Git Logo](/src/assets/Hackathon_Logo.png)