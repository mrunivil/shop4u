import { Shop } from './shop';
import { User } from './user';

export interface ShoppingList {
  id: string;
  shop?: Shop;
  user: User;
  items: string[];
}
