export interface Shop {
  id: string;
  name: string;
  address: string;
  description: string;
  opening: string;
  isHelper: boolean;
  creator: string;
}
