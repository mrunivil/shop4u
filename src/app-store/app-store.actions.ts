import { Shop } from 'src/model/shop';
import { User } from 'src/model/user';
import { ShoppingList } from 'src/model/shoppingList';

export class DoneShopping {
  static readonly type = '[APP] DONE SHOPPING';
  constructor(public payload: string[]) {}
}

export class OrderInMarket {
  static readonly type = '[APP] SAVE ORDER IN FIREBASE';
}

export class AskForHelper {
  static readonly type = '[APP] ASK FOR HELPER';
}

export class SelectShop {
  static readonly type = '[APP] SELECT SHOP';
  constructor(public payload: Shop) {}
}

export class SendOrder {
  static readonly type = '[APP] SEND ORDER';
}

export class LoginUser {
  static readonly type = '[APP] LOGIN USER';
  constructor(public payload: User) {}
}

export class FetchUser {
  static readonly type = '[APP] FETCH USER';
  constructor(public payload: User) {}
}

export class FetchShops {
  static readonly type = '[APP] FETCH SHOPS';
  constructor() {}
}

export class SaveShop {
  static readonly type = '[APP] SAVE SHOP';
  constructor(public payload: Shop) {}
}

export class SelectRequest {
  static readonly type = '[APP] SELECT REQUEST';
  constructor(public payload: ShoppingList) {}
}

export class SelectList {
  static readonly type = '[APP] SELECT LIST';
  constructor(public payload: ShoppingList) {}
}
