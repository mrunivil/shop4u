import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Shop } from 'src/model/shop';
import { ShoppingList } from 'src/model/shoppingList';
import {
  AskForHelper,
  DoneShopping,
  OrderInMarket,
  SelectShop,
  SendOrder,
  LoginUser,
  FetchShops,
  SaveShop,
  SelectList,
  SelectRequest,
} from './app-store.actions';
import { User } from 'src/model/user';

export interface AppStoreStateModel {
  latestOrder: ShoppingList;
  items: string[];
  shopList: Shop[];
  selectedShop?: Shop;
  loggedInUser: User;
  myLists: ShoppingList[];
  myRequests: ShoppingList[];
  selectedRequest?: ShoppingList;
  selectedList?: ShoppingList;
}

@State<AppStoreStateModel>({
  name: 'appStore',
  defaults: {
    latestOrder: {} as ShoppingList,
    items: [],
    shopList: [
      // {
      //   id: '1',
      //   address: 'Schloßallee 1',
      //   description: 'Aldi Süd',
      //   opening: 'Mo - Sa 08:00 - 20:00',
      //   name: 'Aldi',
      //   creator: 'BCOwMKmNTBXFfTARcPYa4Agc2Pe2',//Dennis testid
      // } as Shop,
      // {
      //   address: 'Schloßallee 2',
      //   description:
      //     'Ich erledige deine Bestellungen und bringe sie dir nach Hause',
      //   opening: 'Mo - Sa 10:00 - 18:00',
      //   name: 'John Doe',
      //   creator: 'BCOwMKmNTBXFfTARcPYa4Agc2Pe2',//Dennis testid
      //   isHelper: true
      // } as Shop
    ],
    loggedInUser: { displayName: 'NO USER' } as User,
    myLists: [],
    myRequests: []
  }
})
export class AppStoreState {
  constructor(private router: Router, private firestore: AngularFirestore) {
  }

  @Selector()
  static shopList({ shopList }: AppStoreStateModel) {
    return shopList.filter(s => s.isHelper === undefined);
  }

  @Selector()
  static helpersList({ shopList }: AppStoreStateModel) {
    return shopList.filter(s => s.isHelper === true);
  }

  @Selector()
  static selectedShop({ selectedShop }: AppStoreStateModel) {
    return selectedShop;
  }

  @Selector()
  static items({ items }: AppStoreStateModel) {
    return items;
  }

  @Selector()
  public static getState(state: AppStoreStateModel) {
    return state;
  }

  @Selector()
  public static getUser({ loggedInUser }: AppStoreStateModel) {
    return loggedInUser;
  }

  @Selector()
  public static getMyLists({ myLists }: AppStoreStateModel) {
    return myLists;
  }

  @Selector()
  public static getMyRequests({ myRequests }: AppStoreStateModel) {
    return myRequests;
  }

  @Selector()
  static selectedRequest({ selectedRequest }: AppStoreStateModel) {
    return selectedRequest;
  }

  @Selector()
  static selectedList({ selectedList }: AppStoreStateModel) {
    return selectedList;
  }

  @Action(DoneShopping)
  doneShopping(
    { patchState }: StateContext<AppStoreStateModel>,
    { payload }: DoneShopping
  ) {
    patchState({
      items: payload
    });
    this.router.navigate(['/shopping-done']);
  }

  @Action(OrderInMarket)
  saveOrderForMarket() {
    this.router.navigate(['/shop']);
  }

  @Action(AskForHelper)
  askForHelper() {
    this.router.navigate(['/helper']);
  }

  @Action(SelectShop)
  selectShop(
    { patchState }: StateContext<AppStoreStateModel>,
    { payload }: SelectShop
  ) {
    patchState({
      selectedShop: payload
    });
    this.router.navigate(['/shop',payload.id]);
  }

  @Action(SendOrder)
  sendOrder(ctx: StateContext<AppStoreStateModel>) {
    const state = ctx.getState();
    const shoppingListID = this.firestore.createId();
    const newList = {
      id: shoppingListID,
      items: state.items,
      user: state.loggedInUser,
      shop: state.selectedShop,
      creator: state.loggedInUser.uid,
      resolver: state.selectedShop.creator
    };
    this.firestore.collection('shoppingList').doc<ShoppingList>(shoppingListID).set(newList);
    this.router.navigate(['/send']);
  }

  @Action(LoginUser)
  loginUser(
    { patchState }: StateContext<AppStoreStateModel>,
    { payload }: LoginUser
  ) {
    patchState({
      loggedInUser: payload
    });
    this.firestore.collection<ShoppingList>('shoppingList', ref => ref.where('user', '==', payload)).valueChanges().subscribe(list => {
      patchState({
        myLists: list
      });
    })
    this.firestore.collection<ShoppingList>('shoppingList', ref => ref.where('resolver', '==', payload.uid)).valueChanges().subscribe(list => {
      patchState({
        myRequests: list
      });
    })
    this.router.navigate(['/shopping-done']);
  }

  @Action(FetchShops)
  fetchShops({ patchState }: StateContext<AppStoreStateModel>) {
    this.firestore.collection<Shop>('shops').valueChanges().subscribe(list => {
      patchState({
        shopList: list
      });
    })
  }

  @Action(SaveShop)
  saveShop({ patchState }: StateContext<AppStoreStateModel>, { payload }: SaveShop, ) {
    this.firestore.collection('shops').doc(payload.id).set(payload);
    this.router.navigate(['/dashboard']);
  };

  @Action(SelectRequest)
  selectRequest(
    { patchState }: StateContext<AppStoreStateModel>,
    { payload }: SelectRequest
  ) {
    patchState({
      selectedRequest: payload
    });
    this.router.navigate(['/my-requests', payload.id]);
  }

  @Action(SelectList)
  selectList(
    { patchState }: StateContext<AppStoreStateModel>,
    { payload }: SelectList
  ) {
    patchState({
      selectedList: payload
    });
    this.router.navigate(['/my-lists', payload.id]);
  }
}
