import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Observable } from 'rxjs';
import { ShoppingList } from 'src/model/shoppingList';
import { Router } from '@angular/router';
import { SelectRequest } from 'src/app-store/app-store.actions';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.scss']
})
export class MyRequestsComponent {
  @Select(AppStoreState.getMyRequests) mylists$: Observable<ShoppingList[]>;
  constructor(private store: Store) {

  }

  viewList(list: ShoppingList) {
    this.store.dispatch(new SelectRequest(list))
  }

}
