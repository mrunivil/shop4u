import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select } from '@ngxs/store';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Observable } from 'rxjs';
import { ShoppingList } from 'src/model/shoppingList';

@Component({
  selector: 'app-my-requests-details',
  templateUrl: './my-requests-details.component.html',
  styleUrls: ['./my-requests-details.component.scss']
})
export class MyRequestsDetailsComponent{

  @Select(AppStoreState.selectedRequest) item$: Observable<ShoppingList>;
  
  constructor( ) {
    
  }


}
