import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyRequestsComponent } from './my-requests.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MyRequestsDetailsComponent } from './my-requests-details/my-requests-details.component';



@NgModule({
  declarations: [MyRequestsComponent, MyRequestsDetailsComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyRequestsComponent
      },
      {
        path: ':id',
        component: MyRequestsDetailsComponent
      }
    ])
  ]
})
export class MyRequestsModule { }
