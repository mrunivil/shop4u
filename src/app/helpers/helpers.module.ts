import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HelpersDetailsComponent } from './helpers.details';
import { HelperListComponent } from './helpers.list';
import { NewHelperPage } from './new/newHelper.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    IonicModule, 
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HelperListComponent
      },
      {
        path: '/:id',
        component: HelpersDetailsComponent
      },
      {
        path: 'new',
        component: NewHelperPage
      }
    ])
  ],
  declarations: [HelpersDetailsComponent, HelperListComponent, NewHelperPage]
})
export class HelpersModule { }
