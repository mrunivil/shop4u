import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SendOrder } from 'src/app-store/app-store.actions';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Shop } from 'src/model/shop';

@Component({
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-buttons slot="start" defaultHref="home">
          <ion-back-button def></ion-back-button>
        </ion-buttons>
        <ion-title>{{ (shop$ | async)?.name }}</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-item>
        <ion-label>
          Adresse:
        </ion-label>
        <ion-label class="ion-text-wrap">
          {{ (shop$ | async)?.address }}
        </ion-label>
      </ion-item>
      <ion-item>
        <ion-label>
          Beschreibung:
        </ion-label>
        <ion-label class="ion-text-wrap">
          {{ (shop$ | async)?.description }}
        </ion-label>
      </ion-item>
      <ion-item>
        <ion-label>
          Annahmezeiten
        </ion-label>
        <ion-label class="ion-text-wrap">
          {{ (shop$ | async)?.opening }}
        </ion-label>
      </ion-item>
    </ion-content>
    <ion-footer>
      <ion-button color="success" expand="full" (click)="shopHere()">
        <ion-icon slot="end" class="ion-padding-start" name="send"></ion-icon>
        <ion-label>Um Unterstützung bitten</ion-label></ion-button
      >
    </ion-footer>
  `,
  styles: []
})
export class HelpersDetailsComponent {
  @Select(AppStoreState.selectedShop) shop$: Observable<Shop>;
  constructor(private store: Store) {}

  shopHere() {
    this.store.dispatch(new SendOrder());
  }
}
