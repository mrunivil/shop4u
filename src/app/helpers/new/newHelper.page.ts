import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Shop } from 'src/model/shop';
import { Store } from '@ngxs/store';
import { User } from 'src/model/user';
import { SaveShop } from 'src/app-store/app-store.actions';

@Component({
  selector: 'app-home',
  templateUrl: 'newHelper.page.html',
  styleUrls: ['newHelper.page.scss'],
})
export class NewHelperPage {
  newHelperForm;
  constructor(private formBuilder: FormBuilder, private firestore: AngularFirestore, private router: Router, private store: Store) {
    this.newHelperForm = this.formBuilder.group({
      name: '',
      address: '',
      description: '',
      opening: ''
    });
  }

  submit() {
    const user = this.store.selectSnapshot<User>(state => state.appStore.loggedInUser)
    const uniqueID = this.firestore.createId();
    const insertShop: Shop = { ...{ id: uniqueID, creator: user.uid, isHelper: true }, ...this.newHelperForm.value, }
    this.store.dispatch(new SaveShop(insertShop as Shop));
    this.newHelperForm.reset();
  }
}
