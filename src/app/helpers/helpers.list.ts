import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SelectShop, FetchShops } from 'src/app-store/app-store.actions';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Shop } from 'src/model/shop';

@Component({
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-buttons slot="start" defaultHref="home">
          <ion-back-button def></ion-back-button>
        </ion-buttons>
        <ion-title>Leute deiner Nähe</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <ion-item
          *ngFor="let shop of shops$ | async"
          detail="true"
          (click)="selectShop(shop)"
        >
          <ion-label>
            {{ shop.name }}
          </ion-label>
        </ion-item>
      </ion-list>
    </ion-content>
  `,
  styles: [``]
})
export class HelperListComponent {
  @Select(AppStoreState.helpersList) shops$: Observable<Shop[]>;

  constructor(private store: Store) {
    this.store.dispatch(new FetchShops());
  }

  selectShop(shop: Shop) {
    this.store.dispatch(new SelectShop(shop));
  }
}
