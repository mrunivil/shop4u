import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  template: `
    <ion-header transcluent>
      <ion-toolbar>
        <ion-title>Dein Dashboard</ion-title>
        <ion-buttons slot="secondary">
          <ion-button (click)="authService.logout()">
            <ion-icon slot="icon-only" name="exit"></ion-icon>
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content [fullscreen]="true">
      <div class="outer-container">
        <div class="inner-container">
          <ion-button fill="outline"
          color="success" [routerLink]="['/my-requests']"
            >Neue Anfragen an mich</ion-button
          >
          <ion-button [routerLink]="['/shopping-list']"
            >Neuer Einkauf</ion-button
          >
          <ion-button [routerLink]="['/my-lists']"
            >Vergangene Einkäufe</ion-button
          >
          <ion-button fill="outline" color="tertiary" [routerLink]="['/helper/new']"
            >Als Helfer registrieren</ion-button
          >
          <ion-button
            fill="outline"
            color="tertiary"
            [routerLink]="['/shop/new']"
            >Mein Geschäft registrieren</ion-button
          >
        </div>
      </div>
    </ion-content>
  `,
  styles: [
    `
      .outer-container {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
      }
    `,
    `
      .inner-container {
        padding: 1rem;
        display: flex;
        justify-content: space-evenly;
        height: 90%;
        width: 100%;
        flex-direction: column;
      }
    `,
    `
      ion-button {
        --padding-top: 3rem;
        --padding-bottom: 3rem;
      }
    `
  ]
})
export class DashboardComponent {
  constructor(public authService: AuthService) { }
}
