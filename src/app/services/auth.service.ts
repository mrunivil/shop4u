import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { LoginUser } from 'src/app-store/app-store.actions';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private router: Router,
    public afAuth: AngularFireAuth,
    private store: Store// Inject Firebase auth service
  ) { }

  // Sign in with Google
  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          console.log(res)
          resolve(res);
          this.store.dispatch(new LoginUser({ uid: res.user.uid, email: res.user.email, displayName: res.user.displayName }))
          this.router.navigate(['/dashboard'])
        })
    })
  }

  doAnonymouslyLogin() {
    this.afAuth.auth.signInAnonymously().then(res => {
      console.log(res)
      this.store.dispatch(new LoginUser({ uid: res.user.uid, email: "", displayName: "" }))
      this.router.navigate(['/dashboard'])
    })
  }



  logout() {
    this.afAuth.auth.signOut()
      .then((res) => this.router.navigate(['/home']));
  }

}
// import { Injectable } from '@angular/core';
// import { Router } from "@angular/router";
// import { AngularFireAuth } from "@angular/fire/auth";
// import * as firebase from 'firebase/app';
// import { Observable } from 'rxjs';

// @Injectable()
// export class AuthService {
//   private user: Observable<firebase.User>;
//   private userDetails: firebase.User = null;
//   constructor(private _firebaseAuth: AngularFireAuth, private router: Router) {
//     this.user = _firebaseAuth.authState;
//     this.user.subscribe(
//       (user) => {
//         if (user) {
//           this.userDetails = user;
//           console.log(this.userDetails);
//         }
//         else {
//           this.userDetails = null;
//         }
//       }
//     );
//   }

//   isLoggedIn() {
//     if (this.userDetails == null) {
//       return false;
//     } else {
//       return true;
//     }
//   }

//   logout() {
//     this._firebaseAuth.auth.signOut()
//       .then((res) => this.router.navigate(['/']));
//   }

//   signInWithGoogle() {
//     return this._firebaseAuth.auth.signInWithPopup(
//       new firebase.auth.GoogleAuthProvider()
//     )
//   }
// }