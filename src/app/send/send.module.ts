import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { StartComponent } from './start/start.page';
import { SuccessComponent } from './success/success.page';

@NgModule({
  declarations: [StartComponent, SuccessComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: StartComponent
      },
      {
        path: 'success',
        component: SuccessComponent
      }
    ])
  ]
})
export class SendModule {}
