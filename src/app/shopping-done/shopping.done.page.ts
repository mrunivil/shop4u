import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AskForHelper, OrderInMarket } from 'src/app-store/app-store.actions';
import { AppStoreState } from 'src/app-store/app-store.state';

@Component({
  template: `
    <ion-header
      ><ion-toolbar
        ><ion-title>Wie?</ion-title>
        <ion-buttons slot="start">
          <ion-back-button></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content fullscreen="true">
      <div class="outer-container">
        <div class="inner-container">
          <ion-button (click)="orderInMarket()">Im Markt</ion-button>
          <ion-button (click)="askForHelper()">Durch einen Helfer</ion-button>
          <ion-button (click)="getText()" data-action="share/whatsapp/share"
            >Per Whatsapp Teilen</ion-button
          >
        </div>
      </div>
    </ion-content>
    <ion-footer>
      <ion-item
        ><ion-label class="ion-text-center"
          >{{ (items$ | async)?.length }} Artikel im Warenkorb</ion-label
        ></ion-item
      >
    </ion-footer>
  `,
  styles: [
    `
      .outer-container {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
      }
    `,
    `
      .inner-container {
        padding: 1rem;
        display: flex;
        justify-content: space-evenly;
        height: 90%;
        width: 100%;
        flex-direction: column;
      }
    `,
    `
      ion-button {
        --padding-top: 3rem;
        --padding-bottom: 3rem;
      }
    `
  ]
})
export class ShoppingDoneComponent {
  @Select(AppStoreState.items) items$: Observable<string[]>;

  testurl;
  constructor(private store: Store, private sanitizer: DomSanitizer) {}

  orderInMarket() {
    this.store.dispatch(new OrderInMarket());
  }

  getText() {
    this.items$.subscribe(items => {
      let whatsapptext: string = '';
      whatsapptext = `Hey,\nleider gehöre ich zu einer der Risikogruppen des COVID-19. Könntest du bitte meinen Einkauf für mich erledigen?\nIch benötige folgende Dinge: \n - ${items.join(
        '\n - '
      )}\nDanke! Menschen wie du helfen uns aus der Krise!\n\nErstellt von Shop4U!\nBesuch unseren Prototypen unter https://shop4u.tk`;
      this.testurl = 'whatsapp://send?text=' + encodeURIComponent(whatsapptext);
      window.location.href = this.testurl;
    });
  }

  askForHelper() {
    this.store.dispatch(new AskForHelper());
  }
}
