import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ShoppingDoneComponent } from './shopping.done.page';

@NgModule({
  declarations: [ShoppingDoneComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: ShoppingDoneComponent }]),
    CommonModule,
    IonicModule
  ]
})
export class ShoppingDoneModule {}
