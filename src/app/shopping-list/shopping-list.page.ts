import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { DoneShopping } from 'src/app-store/app-store.actions';


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.page.html',
  styleUrls: ['./shopping-list.page.scss']
})
export class ShoppingListPage {
  items: string[];
  itemName: string;

  constructor(
    private store: Store
  ) {
    this.items = [];
    this.itemName = '';
  }

  addItem() {
    this.items.push(this.itemName);
    this.itemName = '';
  }

  removeObject(i: number) {
    this.items.splice(i, 1);
  }

  done() {
    this.store.dispatch(new DoneShopping(this.items));
  }
}
