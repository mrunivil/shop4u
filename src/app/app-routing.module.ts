import { NgModule } from '@angular/core';
import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const redirectUnauthorizedToLanding = redirectUnauthorizedTo(['home']);

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'helper',
    loadChildren: () =>
      import('./helpers/helpers.module').then(m => m.HelpersModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'shop',
    loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then(m => m.HomePageModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'send',
    loadChildren: () => import('./send/send.module').then(m => m.SendModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'shopping-done',
    loadChildren: () =>
      import('./shopping-done/shopping.done.module').then(
        m => m.ShoppingDoneModule
      ),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'shopping-list',
    loadChildren: () =>
      import('./shopping-list/shopping-list.module').then(
        m => m.ShoppingListPageModule
      ),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'my-lists',
    loadChildren: () =>
      import('./my-lists/my-lists.module').then(m => m.MyListsModule),
    ...canActivate(redirectUnauthorizedToLanding)
  },
  {
    path: 'my-requests',
    loadChildren: () =>
      import('./my-requests/my-requests.module').then(m => m.MyRequestsModule),
    ...canActivate(redirectUnauthorizedToLanding)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
