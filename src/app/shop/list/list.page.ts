import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SelectShop, FetchShops } from 'src/app-store/app-store.actions';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Shop } from 'src/model/shop';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})
export class ListPage {
  @Select(AppStoreState.shopList) shops$: Observable<Shop[]>;

  constructor(private store: Store) {
    this.store.dispatch(new FetchShops());
  }

  selectShop(shop: Shop) {
    this.store.dispatch(new SelectShop(shop));
  }
}
