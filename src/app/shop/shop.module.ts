import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NewShopPage } from './new/newShop.page';
import { ListPage } from './list/list.page';
import { DetailsPage } from './details/details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListPage
      },
      {
        path: 'new',
        component: NewShopPage
      },
      {
        path: ':id',
        component: DetailsPage
      }
    ])
  ],
  declarations: [NewShopPage, ListPage, DetailsPage]
})
export class ShopModule {}
