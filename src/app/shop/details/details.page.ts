import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SendOrder } from 'src/app-store/app-store.actions';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Shop } from 'src/model/shop';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss']
})
export class DetailsPage {
  @Select(AppStoreState.selectedShop) shop$: Observable<Shop>;
  constructor(private store: Store) {
    // this.shopID = this.route.snapshot.paramMap.get('id');
    // this.shop$ = this.firestore.collection('shops').doc<Shop>(this.shopID).valueChanges();
    // this.latestList = this.store.selectSnapshot<ShoppingList>(state => state.appStore.latestOrder)
  }

  shopHere() {
    this.store.dispatch(new SendOrder());
    // this.shop$.subscribe(shop => {
    //   const order = { ...this.latestList, ...{ shop: shop } }
    //   this.firestore.collection('shoppingList').doc(order.id).set(order);
    //   console.dir(order);
    //   this.router.navigate(['dashboard']);
    // })
  }
}
