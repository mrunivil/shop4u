import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ShoppingList } from 'src/model/shoppingList';
import { Observable } from 'rxjs';
import { AppStoreState } from 'src/app-store/app-store.state';
import { User } from 'src/model/user';
import { Router } from '@angular/router';
import { SelectList } from 'src/app-store/app-store.actions';

@Component({
  selector: 'app-my-lists',
  templateUrl: './my-lists.component.html',
  styleUrls: ['./my-lists.component.scss']
})
export class MyListsComponent {
  @Select(AppStoreState.getUser) user$: Observable<User>;
  @Select(AppStoreState.getMyLists) mylists$: Observable<ShoppingList[]>;
  
  constructor(private store: Store) {

  }

  viewList(list: ShoppingList) {
    this.store.dispatch(new SelectList(list))
  }

}
