import { Component} from '@angular/core';
import { Select } from '@ngxs/store';
import { AppStoreState } from 'src/app-store/app-store.state';
import { Observable } from 'rxjs';
import { ShoppingList } from 'src/model/shoppingList';

@Component({
  selector: 'app-my-lists-details',
  templateUrl: './my-lists-details.component.html',
  styleUrls: ['./my-lists-details.component.scss']
})
export class MyListsDetailsComponent{
  @Select(AppStoreState.selectedList) item$: Observable<ShoppingList>;
  constructor( ) {
    
  }

}
