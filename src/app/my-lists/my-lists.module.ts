import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MyListsComponent } from './my-lists.component';
import { MyListsDetailsComponent } from './my-lists-details/my-lists-details.component';

@NgModule({
  declarations: [MyListsComponent, MyListsDetailsComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyListsComponent
      },
      {
        path: ':id',
        component: MyListsDetailsComponent
      }
    ])
  ]
})
export class MyListsModule { }
